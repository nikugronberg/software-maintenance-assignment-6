
def readCSV(name):
    linelist = []
    try:
        f = open(name, "r")
        for line in f:
            linelist.append(line)
    except OSError as e:
        print(e)
    return linelist

def compareLists(list1, list2):
    print("List 1 length:", len(list1), "List 2 length", len(list2))
    length = len(list1) if len(list1) > len(list2) else len(list2)

    similarityList = set(list1).intersection(list2)

    return len(similarityList) / length

def main():
    pyWords = readCSV("outputs/pyWords.csv")
    pySentences = readCSV("outputs/pySentences.csv")
    pycWords = readCSV("outputs/pycWords.csv")
    pycSentences = readCSV("outputs/pycSentences.csv")

    similarityWords = compareLists(pyWords, pycWords)
    similaritySentences = compareLists(pySentences, pycSentences)

    print("Similarity between word files:", similarityWords)
    print("Similarity between sentence files:", similaritySentences)

if __name__ == '__main__':
    main()
